Seguir este tutorial:

https://www.youtube.com/watch?v=Zy89Nj7tNNM


npm init --yes
npm i express
npm i nodemon -D

Consideraciones generales para este y otros proyectos:

- verificar que nodemon se instale como desarrollo
- verificar de utilizar path para mejor orden
- verificar que en cada nuevo archivo que necesite ser llamado del index, debe requerir express
- 