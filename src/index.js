const express = require('express')
const engine = require('ejs-mate')
const path = require('path')
var log4js = require('log4js');


log4js.configure({
    appenders: {
      console: { type: 'console' },
      file: { type: 'file', filename: 'app.log' }
    },
    categories: {
      cheese: { appenders: ['file'], level: 'info' },
      default: { appenders: ['console'], level: 'info' },
      full:{appenders:['console', 'file'], level: 'info' }
    }
   });
//Initializations
const app = express();


//Setting
app.engine('ejs', engine)
app.set('view engine', 'ejs')
const dir_view=path.join(__dirname,'views');
console.log(dir_view)
app.set('views',dir_view)

var logger = log4js.getLogger('default');
//Starting the server
const port =3000;
app.listen(port, ()=>{

    console.log("servidor iniciado, escuchando en puerto",port)
})

//Routes
app.use(require ('./routes/'));
app.use(log4js.connectLogger(logger, { level: 'info' }));
//Static files
const dir_public=path.join(__dirname,'public');
console.log(dir_public)
app.use(express.static( path.join(__dirname,'public')))